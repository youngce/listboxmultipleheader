﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ReactiveUI.Xaml;

namespace ListBoxMultipleHeader
{
    public class EmployeItem
    {
        public int Id { get; set; }
        public int Age { get; set; }
        public string Adresse { get; set; }

        public static ObservableCollection<EmployeItem> GenEmployeItems(int genCount)
        {
            var items = new ObservableCollection<EmployeItem>();
            var rand = new Random();
            
            for (int i = 0; i < genCount; i++)
            {
                var item = new EmployeItem
                {
                    Id=rand.Next(0,(int)(Math.Pow(10,4))),
                    Age = rand.Next(0,100),
                    Adresse = string.Format("Adresse{0}",i)
                };
                items.Add(item);

            }
            return items;
        }
    }

    public class FormViewModel
    {
        public string[] Headers { get; set; }
        public ObservableCollection<EmployeItem> Items { get;private set; }
        public FormViewModel()
        {
            Items = EmployeItem.GenEmployeItems(10);
        }
    }
    public class MainWindowsViewModel
    {
        public FormViewModel Form { get; set; }
        public ReactiveCommand Trasfer { get; set; }
        public MainWindowsViewModel()
        {
            Form=new FormViewModel();
            Trasfer=new ReactiveCommand();
            Trasfer.Subscribe(TransferCommand);
        }

        public void TransferCommand(object o)
        {
            
        }
    }
}
